const THEME = require(`./src/utils/theme`);

require('dotenv').config({
  path: `.env`,
});

module.exports = {
  siteMetadata: {
    title: `Luke Patton`,
    description: `London based Creative & Design Director. I concept, I design and I create internet stuff.`,
    author: `Piers Olenski`,
    siteUrl: `https://luke-patton.netlify.com`,
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-eslint',
      options: {
        options: {
          emitWarning: true,
          failOnError: false,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-transition-link',
      options: {
        layout: require.resolve(`./src/components/common/Layout.js`),
        injectPageProps: false,
      },
    },
    {
      resolve: `gatsby-source-prismic`,
      options: {
        repositoryName: `luke-patton`,
        accessToken: `${process.env.PRISMIC_API_KEY}`,
        linkResolver: () => post => `/${post.uid}`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-svg-sprite`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-robots-txt`,
    `gatsby-plugin-sitemap`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Luke Patton`,
        short_name: `Luke Patton`,
        start_url: `/`,
        background_color: THEME.colors.black,
        theme_color: THEME.colors.white,
        display: `minimal-ui`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-132163844-1`,
        head: false,
      },
    },
  ],
};
