const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const pages = await graphql(`
    {
      allPrismicProject {
        edges {
          node {
            id
            uid
          }
        }
      }
    }
  `);

  const template = path.resolve('src/templates/project.js');

  pages.data.allPrismicProject.edges.forEach(edge => {
    createPage({
      path: `/work/${edge.node.uid}`,
      component: template,
      context: {
        uid: edge.node.uid,
      },
    });
  });
};
