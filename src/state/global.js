import { useState } from 'react';
import { createContainer } from 'unstated-next';

function state() {
  const [hovering, toggleHovering] = useState(false);
  const [cursorText, setCursorText] = useState('');
  const [objLoaded, setObjLoaded] = useState(false);
  const [bustEngaged, setBustEngaged] = useState(false);
  const [pageActive, setPageActive] = useState(true);
  const [thumbsLoaded, setThumbsLoaded] = useState(false);
  return {
    objLoaded,
    bustEngaged,
    setObjLoaded,
    setBustEngaged,
    hovering,
    toggleHovering,
    cursorText,
    setCursorText,
    pageActive,
    setPageActive,
    thumbsLoaded,
    setThumbsLoaded,
  };
}

export default createContainer(state);
