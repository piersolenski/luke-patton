import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import SEO from '../components/common/Seo';
import Entry from '../components/journal/Entry';
import Hero from '../components/journal/Hero';
import Entries from '../components/journal/Entries';

function Journal({ data: { allPrismicPost, prismicJournal } }) {
  return (
    <>
      <SEO title="Journal" />
      <Hero text={prismicJournal.data.title.text} />
      <Entries>
        {allPrismicPost.edges.map(project => (
          <Entry
            key={project.node.id}
            date={project.node.data.date}
            title={project.node.data.title.text}
            media={project.node.data.body}
          />
        ))}
      </Entries>
    </>
  );
}

export default Journal;

export const pageQuery = graphql`
  query {
    prismicJournal {
      data {
        title {
          text
        }
      }
    }
    allPrismicPost {
      edges {
        node {
          id
          data {
            title {
              text
            }
            date
            body {
              ... on PrismicPostBodyImage {
                id
                slice_type
                primary {
                  image {
                    alt
                    localFile {
                      childImageSharp {
                        fluid(maxWidth: 1200, quality: 80) {
                          ...GatsbyImageSharpFluid_withWebp
                        }
                      }
                    }
                  }
                }
              }
              ... on PrismicPostBodyVideo {
                id
                slice_type
                primary {
                  autoplay
                  video {
                    url
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

Journal.propTypes = {
  data: PropTypes.object.isRequired,
};
