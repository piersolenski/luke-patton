import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import SEO from '../components/common/Seo';
import Waypoint from '../components/common/Waypoint';
import Bust from '../components/home/Bust';
import Thumbnails from '../components/home/Thumbnails';
import Headline from '../components/home/Headline';
import Loading from '../components/home/Loading';
import Wrapper from '../components/home/Wrapper';

import state from '../state/global';

export default function Home({ data }) {
  return (
    <Waypoint
      bottomOffset="0%"
      render={active => {
        // const { objLoaded, thumbsLoaded, bustEngaged } = state.useContainer();
        // const assetsLoaded = objLoaded && thumbsLoaded;
        const { objLoaded, bustEngaged } = state.useContainer();
        const assetsLoaded = objLoaded;
        return (
          <Wrapper>
            <SEO title={data.page.data.title_line_1.text} />
            <Thumbnails
              thumbs={data.page.data.body}
              active={active && assetsLoaded}
              blur={bustEngaged}
            />
            <Headline
              active={active && assetsLoaded && !bustEngaged}
              lines={[
                data.page.data.title_line_1.text,
                data.page.data.title_line_2.text,
              ]}
            />
            <Bust active={active && assetsLoaded} />
            {!assetsLoaded && <Loading active={active} />}
          </Wrapper>
        );
      }}
    />
  );
}

export const pageQuery = graphql`
  query {
    page: prismicHome {
      data {
        title_line_1 {
          text
        }
        title_line_2 {
          text
        }
        body {
          ... on PrismicHomeBodyImage {
            id
            slice_type
            primary {
              column_span
              column_start
              image {
                alt
                localFile {
                  childImageSharp {
                    fluid(
                      maxWidth: 320
                      maxHeight: 180
                      cropFocus: CENTER
                      quality: 80
                    ) {
                      ...GatsbyImageSharpFluid_withWebp
                    }
                  }
                }
              }
            }
          }
          ... on PrismicHomeBodyVideo {
            id
            slice_type
            primary {
              column_span
              column_start
              video {
                url
              }
            }
          }
        }
      }
    }
  }
`;

Home.propTypes = {
  data: PropTypes.object.isRequired,
};
