import React from 'react';

import Main from '../components/404/Main';
import SEO from '../components/common/Seo';

const NotFoundPage = () => (
  <div>
    <SEO title="Page not found" />
    <Main />
  </div>
);

export default NotFoundPage;
