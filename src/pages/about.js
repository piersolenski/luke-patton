import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import Hero from '../components/about/Hero';
import Main from '../components/about/Main';
import SEO from '../components/common/Seo';

const About = ({ data: { prismicAbout } }) => (
  <div>
    <SEO title="About" />
    <Hero
      title1={prismicAbout.data.title_line_1.text}
      title2={prismicAbout.data.title_line_2.text}
      portrait={prismicAbout.data.portrait.localFile.childImageSharp.fluid}
      intro1={prismicAbout.data.paragraph_1.text}
      intro2={prismicAbout.data.paragraph_2.text}
    />
    <Main
      clients={prismicAbout.data.clients}
      awards={prismicAbout.data.awards___recognition}
      socialMedia={prismicAbout.data.social_media}
      email={prismicAbout.data.email}
      phone={prismicAbout.data.phone}
    />
  </div>
);

export const pageQuery = graphql`
  query {
    prismicAbout {
      data {
        title_line_1 {
          text
        }
        title_line_2 {
          text
        }
        paragraph_1 {
          text
        }
        paragraph_2 {
          text
        }
        portrait {
          localFile {
            childImageSharp {
              fluid(maxWidth: 600, quality: 80) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        clients {
          client
        }
        awards___recognition {
          body
        }
        social_media {
          name
          url {
            url
          }
        }
        email
        phone
        footer_cta {
          text
        }
      }
    }
  }
`;

About.propTypes = {
  data: PropTypes.object.isRequired,
};

export default About;
