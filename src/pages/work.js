import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import SEO from '../components/common/Seo';
import Slider from '../components/work/Slider';

function Work({ data }) {
  return (
    <>
      <SEO title="Work" />
      <Slider projects={data.allPrismicProject.edges} />
    </>
  );
}

export default Work;

export const pageQuery = graphql`
  {
    allPrismicProject(sort: { fields: data___date, order: DESC }) {
      edges {
        node {
          id
          uid
          data {
            title {
              text
            }
            strapline {
              text
            }
            date
            preview_image {
              localFile {
                childImageSharp {
                  fluid(
                    maxWidth: 950
                    maxHeight: 528
                    cropFocus: CENTER
                    quality: 80
                  ) {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            preview_video {
              url
            }
          }
        }
      }
    }
  }
`;

Work.propTypes = {
  data: PropTypes.object.isRequired,
};
