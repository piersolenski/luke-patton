import { keyframes } from 'styled-components';

export const scrollDown = keyframes`
 0% {
    transform:  translateY(-100%);
  }
  100% {
    transform: translateY(100%);
  }
`;
