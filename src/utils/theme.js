module.exports = {
  colors: {
    white: '#fff',
    black: '#000',
    grey: '#808080',
    green: '#00FF06',
  },
  breakpoints: {
    mobileSmall: 320,
    mobile: 320,
    phablet: 600,
    tabletLandscape: 1100,
    laptop: 1200,
    desktop: 1400,
  },
};
