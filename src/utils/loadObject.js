import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';

export default function loadObject(url, callback) {
  const objectLoader = new OBJLoader();
  objectLoader.load(url, obj => callback(obj));
}
