export function addStyle(property, value) {
  return document.documentElement.style.setProperty(property, value);
}

export function getStyle(property) {
  if (typeof window === 'undefined') return null;
  return getComputedStyle(document.documentElement).getPropertyValue(property);
}

export function removeStyle(property) {
  return document.documentElement.style.removeProperty(property);
}
