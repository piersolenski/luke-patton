export default function getNextProject(projects, currentSlug) {
  const index = projects.findIndex(project => project.node.uid === currentSlug);
  const nextProject =
    index === projects.length - 1 ? projects[0] : projects[index + 1];
  return nextProject;
}
