import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

  :root {
    --mouse-x: 0px;
    --mouse-y: 0px;
    --site-foreground: ${({ theme }) => theme.colors.white};
    --site-background: ${({ theme }) => theme.colors.black};
    --animation-duration: .25;
    --dur-short: calc(var(--animation-duration) * 1s);
    --dur-medium: calc(var(--animation-duration) * 2s);
    --dur-long: calc(var(--animation-duration) * 3s);
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
    background-repeat: no-repeat;
  }

  *, *:before, *:after {
    cursor: none !important;
    &:hover, &:focus {
      cursor: none !important;
    }
  }

  html {
    font-family: PxGrotesk, sans-serif;
    -webkit-font-smoothing: antialiased;
    overflow-y: scroll;
    font-size: 17px;
    line-height: 25px;
    color: var(--site-foreground);
    background: var(--site-background);
    transition: background .8s ease;
  }

  body {
    margin: 0;
  }

  audio,
  canvas,
  iframe,
  img,
  svg,
  video  {
    max-width: 100%;
    vertical-align: middle;
  }

  a {
    text-decoration: none;
  }

  textarea {
    resize: vertical;
  }

  ::selection {
    color: ${({ theme }) => theme.colors.black};
    background: ${({ theme }) => theme.colors.green};
  }

  /* Width */
  ::-webkit-scrollbar {
    width: 2px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.black};
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.green}; 
  }

  .js-focus-visible :focus:not(.focus-visible) {
    outline: none;
  }

  .tl-edges {
    overflow-x: initial;
  }

  .tl-wrapper {
    float: none;
  }
`;

export default GlobalStyle;
