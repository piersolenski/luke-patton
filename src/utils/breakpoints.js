import invariant from './invariant';

const error = breakpoint =>
  `Breakpoint "${breakpoint}" does not exist in theme`;

export function max(breakpoint) {
  return function getStyle({ theme }) {
    const value = theme.breakpoints[breakpoint];
    invariant(value, error(breakpoint));
    return `${value - 1}px`;
  };
}

export function min(breakpoint) {
  return function getStyle({ theme }) {
    const value = theme.breakpoints[breakpoint];
    invariant(value, error(breakpoint));
    return `${value}px`;
  };
}
