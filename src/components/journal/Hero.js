import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { H1 } from '../common/Typography';
import Grid from '../common/Grid';
import Unveiler from '../common/Unveiler';
import Waypoint from '../common/Waypoint';

import fluidRange from '../../utils/fluidRange';

const Wrapper = styled(Unveiler)`
  grid-column: 2 / span 10;
  ${fluidRange({
    prop: 'margin-top',
    fromSize: '80px',
    toSize: '280px',
  })};
  ${fluidRange({
    prop: 'margin-bottom',
    fromSize: '60px',
    toSize: '200px',
  })};
  text-align: center;
`;

const Hero = ({ text }) => (
  <Grid>
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Wrapper active={active}>
          <H1>{text}</H1>
        </Wrapper>
      )}
    />
  </Grid>
);

Hero.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Hero;
