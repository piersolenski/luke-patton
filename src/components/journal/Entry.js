import React from 'react';
import styled from 'styled-components';
import { format } from 'date-fns';
import PropTypes from 'prop-types';

import { H2 } from '../common/Typography';
import Fader from '../common/Fader';
import MediaItem from '../common/MediaItem';
import HorizontalRule from '../common/HorizontalRule';
import Unveiler from '../common/Unveiler';
import Waypoint from '../common/Waypoint';

import fluidRange from '../../utils/fluidRange';
import { min, max } from '../../utils/breakpoints';

const Wrapper = styled.article`
  @media (max-width: ${max('tabletLandscape')}) {
    grid-column: span 12;
  }
  @media (min-width: ${min('tabletLandscape')}) {
    grid-column: 2 / span 10;
  }
`;

const Header = styled(Unveiler)`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const Title = styled(H2)`
  margin-right: 2%;
`;

const Time = styled(H2)`
  text-align: right;
`;

const Items = styled.div`
  display: grid;
  margin-top: 35px;
  ${fluidRange({
    prop: 'grid-row-gap',
    fromSize: '10px',
    toSize: '30px',
  })};
`;

function Entry({ title, date, media }) {
  return (
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Wrapper>
          <Header active={active}>
            <Title title>{title}</Title>
            <Time title as="time" dateTime={format(date, 'YYYY-MM-DD')}>
              {format(date, 'D MMM YYYY')}
            </Time>
          </Header>
          <HorizontalRule active={active} />
          <Items>
            {media.map((item, i) => (
              <Waypoint
                key={i}
                bottomOffset="20%"
                render={itemActive => (
                  <Fader active={itemActive}>
                    <MediaItem item={item} />
                  </Fader>
                )}
              />
            ))}
          </Items>
        </Wrapper>
      )}
    />
  );
}

Entry.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  media: PropTypes.array.isRequired,
};

export default Entry;
