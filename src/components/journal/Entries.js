import styled from 'styled-components';

import fluidRange from '../../utils/fluidRange';

import Grid from '../common/Grid';

const Entries = styled(Grid)`
  ${fluidRange({
    prop: 'grid-row-gap',
    fromSize: '60px',
    toSize: '100px',
  })};
  padding-bottom: 60px;
`;

export default Entries;
