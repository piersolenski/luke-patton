import styled from 'styled-components';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  min-height: 100vh;
  height: -webkit-fill-available;
  overflow: hidden;
`;

export default Wrapper;
