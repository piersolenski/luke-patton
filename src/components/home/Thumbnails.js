import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Grid from '../common/Grid';
import MediaItem from '../common/MediaItem';
import Fader from '../common/Fader';

import { scrollDown } from '../../utils/keyframes';
import { min, max } from '../../utils/breakpoints';
import state from '../../state/global';

const Wrapper = styled(Fader)`
  overflow: hidden;
  grid-area: 1 / 1;
  overflow: hidden;
  pointer-events: none;
  position: relative;
`;

const Inner = styled.div`
  --dur-long: ${({ active }) =>
    active && `calc(var(--animation-duration) * 20s)`};
  position: absolute;
  animation: ${scrollDown} ${({ scrollSpeed }) => scrollSpeed}s linear infinite;
  width: 100%;
  &:nth-child(2) {
    animation-delay: -${({ scrollSpeed }) => scrollSpeed / 2}s;
  }
`;

const Thumbnail = styled(MediaItem)`
  grid-column: span 3;
  transition: transform ease var(--dur-long), filter ease var(--dur-long);
  transition-delay: ${({ delay }) => delay}s;
  filter: ${({ blur }) => (blur ? 'blur(5px)' : null)};
  transform: ${({ scale }) => (scale ? 'scale(.9)' : null)};
  will-change: transform, filter;
  grid-row: ${({ row }) => row};
  grid-column: ${({ column }) => column} / span ${({ span }) => span};
  margin-bottom: 20vh;
  @media (max-width: ${max('phablet')}) {
    grid-column: ${({ column }) => (column > 8 ? column - 1 : column)} / span
      ${({ span }) => span + 1};
  }
  @media (min-width: ${min('phablet')}) {
    grid-column: ${({ column }) => column} / span ${({ span }) => span};
  }
`;

export default function Thumbnails({ thumbs, active, blur }) {
  const { setThumbsLoaded } = state.useContainer();
  const [loaded, setLoaded] = useState(1);
  if (thumbs.length === loaded) {
    setThumbsLoaded(true);
  }
  return (
    <Wrapper active={active} delay={1.2}>
      {[...Array(2)].map((_, innerIndex) => (
        <Inner
          active={active}
          key={innerIndex}
          scrollSpeed={thumbs.length * 10}
        >
          <Grid>
            {thumbs.map((thumb, thumbIndex) => (
              <Thumbnail
                key={thumbIndex}
                media={thumb}
                onLoad={() => setLoaded(prevLoaded => prevLoaded + 1)}
                autoPlay
                playsInline
                blur={blur}
                scale={blur}
                item={thumb}
                column={thumb.primary.column_start}
                span={thumb.primary.column_span}
                delay={thumbIndex * 0.001}
                row={thumbIndex + 1}
              />
            ))}
          </Grid>
        </Inner>
      ))}
    </Wrapper>
  );
}

Thumbnails.propTypes = {
  active: PropTypes.bool,
  blur: PropTypes.bool,
  thumbs: PropTypes.array.isRequired,
};
