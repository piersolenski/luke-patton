import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import useTimeout from 'use-timeout';

import state from '../../state/global';

const Wrapper = styled.div`
  position: fixed;
  color: ${({ theme }) => theme.colors.green};
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 12px;
`;

export default function Loading({ className, delay = 500 }) {
  const [progress, setProgress] = useState(0);
  const [visible, setVisible] = useState(false);
  const { setThumbsLoaded, setObjLoaded } = state.useContainer();

  const requestRef = useRef();
  const previousTimeRef = useRef();

  useTimeout(() => setVisible(true), delay);

  const animate = time => {
    if (previousTimeRef.current !== undefined) {
      const deltaTime = time - previousTimeRef.current;
      setProgress(prevCount => (prevCount + deltaTime * 0.01) % 100);
    }
    previousTimeRef.current = time;
    requestRef.current = requestAnimationFrame(animate);
  };

  if (progress > 99) {
    setThumbsLoaded(true);
    setObjLoaded(true);
  }

  useEffect(() => {
    requestRef.current = requestAnimationFrame(animate);
    return () => cancelAnimationFrame(requestRef.current);
  }, []);
  return visible ? (
    <Wrapper className={className}>{Math.round(progress)}%</Wrapper>
  ) : null;
}

Loading.propTypes = {
  className: PropTypes.string,
  delay: PropTypes.number,
};
