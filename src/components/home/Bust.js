import PropTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import {
  Scene,
  WebGLRenderer,
  PerspectiveCamera,
  HemisphereLight,
  Raycaster,
  Vector2,
  PointLight,
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

import state from '../../state/global';
import useWindowSize from '../../hooks/useWindowSize';
import loadObject from '../../utils/loadObject';
import { addStyle, removeStyle } from '../../utils/globalStyleProps';

const Canvas = styled.canvas`
  grid-area: 1/1;
  transition: opacity var(--dur-long) ease, transform var(--dur-long) ease;
  opacity: ${({ active }) => (active ? 1 : 0)};
  touch-action: none;
  transform: ${({ active, engaged }) => {
    if (!active && !engaged) return 'scale(.8)';
    if (active && engaged) return 'scale(1.2)';
    return 'scale(1)';
  }};
`;

export default function Bust({ active }) {
  const canvasRef = useRef();
  const {
    objLoaded,
    bustEngaged,
    setObjLoaded,
    setBustEngaged,
    toggleHovering,
    setCursorText,
  } = state.useContainer();
  const [intersecting, setIntersecting] = useState(false);

  const windowSize = useWindowSize();

  let frameId;
  const cameraRef = useRef();
  const rendererRef = useRef();
  const controlsRef = useRef();
  const obj = useRef();
  const mouse = useRef();
  const raycaster = useRef();

  useEffect(() => {
    start();
    return () => stop();
  }, []);

  useEffect(() => onWindowResize(), [windowSize]);

  function start() {
    const scene = new Scene();
    raycaster.current = new Raycaster();
    mouse.current = new Vector2();
    // Window dimensions
    const { innerWidth: width, innerHeight: height } = window;
    // Set renderer
    rendererRef.current = new WebGLRenderer({
      alpha: true,
      canvas: canvasRef.current,
    });
    const { current: renderer } = rendererRef;
    renderer.setSize(width, height);

    // Set camera
    cameraRef.current = new PerspectiveCamera(30, width / height, 1, 200000);
    const { current: camera } = cameraRef;
    camera.position.set(0, 0, 2020);
    scene.add(camera);

    // Set lights
    const light = new HemisphereLight(0xaaaaaa, 0x222222, 0.5);
    scene.add(light);

    const pointLight = new PointLight(0xbbbbbb, 0.1);
    pointLight.position.set(-500, 500, 500);
    scene.add(pointLight);
    camera.add(pointLight);

    // Orbit controls
    controlsRef.current = new OrbitControls(camera, canvasRef.current);
    const { current: controls } = controlsRef;
    controls.enableZoom = false;
    controls.autoRotate = true;
    controls.enableDamping = true;
    controls.dampingFactor = 0.04;
    controls.autoRotateSpeed = 3;

    // Animation loop
    const animate = () => {
      frameId = window.requestAnimationFrame(animate);
      controls.update();
      renderer.render(scene, camera);
    };

    // Load object
    loadObject('./head.obj', object => {
      setObjLoaded(true);
      obj.current = object;
      scene.add(obj.current);
      renderer.render(scene, camera);
      animate();
    });
  }

  function checkForIntersection(e) {
    e.preventDefault();
    const { domElement } = rendererRef.current;
    const { current: camera } = cameraRef;
    mouse.current.x = (e.clientX / domElement.clientWidth) * 2 - 1;
    mouse.current.y = -(e.clientY / domElement.clientHeight) * 2 + 1;

    raycaster.current.setFromCamera(mouse.current, camera);

    const intersects = raycaster.current.intersectObjects([obj.current], true);

    if (intersects.length > 0) {
      setIntersecting(true);
    } else {
      setIntersecting(false);
    }
  }

  function onWindowResize() {
    const { innerWidth: width, innerHeight: height } = window;
    const { current: renderer } = rendererRef;
    const { current: camera } = cameraRef;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
  }

  function stop() {
    const { current: controls } = controlsRef;
    cancelAnimationFrame(frameId);
    controls.dispose();
  }

  useEffect(() => {
    if (intersecting || bustEngaged) {
      toggleHovering(true);
    } else {
      toggleHovering(false);
    }
  }, [intersecting, bustEngaged]);

  return (
    <Canvas
      ref={canvasRef}
      active={active}
      engaged={bustEngaged}
      onMouseMove={e => objLoaded && checkForIntersection(e)}
      onMouseDown={() => {
        setBustEngaged(true);
        setCursorText('Drag');
        toggleHovering(true);
        addStyle('--dur-long', `var(--dur-medium)`);
      }}
      onMouseUp={() => {
        setBustEngaged(false);
        setCursorText('');
        removeStyle('--dur-long');
      }}
      onTouchStart={() => setBustEngaged(true)}
      onTouchEnd={() => setBustEngaged(false)}
    />
  );
}

Bust.propTypes = {
  active: PropTypes.bool,
};
