import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Grid from '../common/Grid';
import Unveiler from '../common/Unveiler';
import { H1 } from '../common/Typography';

const Wrapper = styled(Grid)`
  pointer-events: none;
  align-self: center;
  text-align: center;
  z-index: 10;
  grid-area: 1 / 1;
  ${Unveiler} {
    grid-column: 1 / -1;
  }
`;

const Line = styled(H1)`
  display: block;
  line-height: 1.25em;
  text-shadow: 0 0 0.2em rgba(0, 0, 0, 0.1);
`;

export default function Headline({ className, lines, active }) {
  return (
    <Wrapper className={className}>
      <Unveiler active={active} delay={0.6}>
        <Line as="h1">{lines[0]}</Line>
      </Unveiler>
      <Unveiler active={active} delay={0.8}>
        <Line as="p">{lines[1]}</Line>
      </Unveiler>
    </Wrapper>
  );
}

Headline.propTypes = {
  active: PropTypes.bool,
  className: PropTypes.string,
  lines: PropTypes.array.isRequired,
};
