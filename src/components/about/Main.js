import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import DiagonalArrow from '../icons/DiagonalArrow';
import Grid from '../common/Grid';
import Hr from '../common/HorizontalRule';
import { H2, UnstyledList } from '../common/Typography';
import Unveiler from '../common/Unveiler';
import Waypoint from '../common/Waypoint';

import fluidRange from '../../utils/fluidRange';
import { min, max } from '../../utils/breakpoints';

const Section = styled(Grid)`
  @media (max-width: ${max('mobile')}) {
    flex-direction: column;
  }
  @media (min-width: ${min('mobile')}) {
    justify-content: space-between;
    align-items: baseline;
  }
  ${fluidRange({
    prop: 'margin-bottom',
    fromSize: '50px',
    toSize: '100px',
  })};
`;

const SpacedHr = styled(Hr)`
  margin-bottom: 15px;
`;

const List = styled(UnstyledList)`
  grid-column: span 6;
  @media (max-width: ${max('mobile')}) {
    width: 50%;
  }
  @media (min-width: ${min('mobile')}) {
    text-align: right;
  }
`;

const Title = styled(Unveiler)`
  grid-column: span 6;
`;

const ListItem = styled(H2)`
  color: ${({ theme }) => theme.colors.grey};
`;

const ItemLink = styled.a`
  color: inherit;
  transition: color var(--dur-short) ease;
  &:hover {
    color: ${({ theme }) => theme.colors.green};
  }
  svg {
    vertical-align: -2px;
    margin-left: 16px;
  }
`;

const Arrow = styled(DiagonalArrow)`
  @media (max-width: ${max('phablet')}) {
    width: 15px;
  }
  @media (min-width: ${min('phablet')}) {
    width: 22px;
  }
`;

const Main = ({ clients, awards, socialMedia, email, phone }) => (
  <>
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Section>
          <SpacedHr active={active} />
          <Title active={active}>
            <H2>Clients</H2>
          </Title>
          <List>
            {clients.map((client, i) => (
              <Unveiler key={i} active={active} delay={i * 0.025}>
                <ListItem as="li">{client.client}</ListItem>
              </Unveiler>
            ))}
          </List>
        </Section>
      )}
    />
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Section>
          <SpacedHr active={active} />
          <Title active={active}>
            <H2>Awards &amp; Recognition</H2>
          </Title>
          <List>
            {awards.map((awarder, i) => (
              <Unveiler key={i} active={active} delay={i * 0.025}>
                <ListItem as="li">{awarder.body}</ListItem>
              </Unveiler>
            ))}
          </List>
        </Section>
      )}
    />
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Section>
          <SpacedHr active={active} />
          <Title active={active}>
            <H2>Website Developer</H2>
          </Title>
          <List>
            <Unveiler active={active}>
              <ListItem as="li">
                <ItemLink
                  href="https://piersolenski.com"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Piers Olenski
                  <Arrow />
                </ItemLink>
              </ListItem>
            </Unveiler>
          </List>
        </Section>
      )}
    />
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Section>
          <SpacedHr active={active} />
          <Title active={active}>
            <H2>Social Media</H2>
          </Title>
          <List>
            {socialMedia.map((lifeSuck, i) => (
              <Unveiler key={i} active={active} delay={i * 0.025}>
                <ListItem as="li">
                  <ItemLink
                    href={lifeSuck.url.url}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {lifeSuck.name}
                    <Arrow />
                  </ItemLink>
                </ListItem>
              </Unveiler>
            ))}
          </List>
        </Section>
      )}
    />
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Section>
          <SpacedHr active={active} />
          <Title active={active}>
            <H2>Contact</H2>
          </Title>
          <List>
            <Unveiler active={active}>
              <ListItem as="li">
                <ItemLink
                  href={`mailto:${email}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {email}
                </ItemLink>
              </ListItem>
            </Unveiler>
            <Unveiler active={active} delay={0.025}>
              <ListItem as="li">
                <ItemLink
                  href={`tel:${phone}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {phone}
                </ItemLink>
              </ListItem>
            </Unveiler>
          </List>
        </Section>
      )}
    />
  </>
);

Main.propTypes = {
  clients: PropTypes.array.isRequired,
  awards: PropTypes.array.isRequired,
  socialMedia: PropTypes.array.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
};

export default Main;
