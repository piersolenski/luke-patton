import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';

import Grid from '../common/Grid';
import { H1, P } from '../common/Typography';
import Fader from '../common/Fader';
import Unveiler from '../common/Unveiler';
import Waypoint from '../common/Waypoint';

import fluidRange from '../../utils/fluidRange';
import { min, max } from '../../utils/breakpoints';

const Top = styled(Grid)`
  @media (max-width: ${max('laptop')}) {
    display: block;
  }
  ${fluidRange({
    prop: 'padding-top',
    fromSize: '80px',
    toSize: '180px',
  })};
  ${fluidRange({
    prop: 'padding-bottom',
    fromSize: '30px',
    toSize: '130px',
  })};
`;

const Line1 = styled(Unveiler)`
  grid-column: 5 / span 8;
  margin-bottom: 80px;
`;

const Line2 = styled(Unveiler)`
  grid-column: 1 / span 8;
`;

const LearnMore = styled(Grid)`
  margin-bottom: 92px;
`;

const Portrait = styled(Fader)`
  @media (max-width: ${max('laptop')}) {
    margin-bottom: 30px;
    grid-column: 1 / span 6;
  }
  @media (min-width: ${min('laptop')}) {
    grid-column: 1 / span 5;
    margin-top: 150px;
  }
`;

const Paragraph1 = styled(Fader)`
  text-indent: 40px;
  @media (max-width: ${max('laptop')}) {
    grid-row: 2;
    grid-column: 3 / span 10;
  }
  @media (min-width: ${min('laptop')}) {
    grid-column: 7 / span 3;
  }
`;

const Paragraph2 = styled(Fader)`
  text-indent: 40px;
  @media (max-width: ${max('laptop')}) {
    grid-row: 3;
    grid-column: 3 / span 10;
  }
  @media (min-width: ${min('laptop')}) {
    grid-column: span 3;
  }
`;

export default function Hero({ title1, title2, portrait, intro1, intro2 }) {
  return (
    <>
      <Waypoint
        bottomOffset="20%"
        render={active => (
          <Top as={H1}>
            <Line1 active={active}>
              <span>{title1}</span>
            </Line1>
            &nbsp;
            <Line2 delay={0.2} active={active}>
              <span>{title2}</span>
            </Line2>
          </Top>
        )}
      />
      <Waypoint
        bottomOffset="20%"
        render={active => (
          <LearnMore>
            <Portrait active={active} delay={0.8}>
              <Img fluid={portrait} alt="Luke Patton" />
            </Portrait>
            <Paragraph1 active={active} delay={0.2}>
              <P>{intro1}</P>
            </Paragraph1>
            <Paragraph2 active={active} delay={0.4}>
              <P>{intro2}</P>
            </Paragraph2>
          </LearnMore>
        )}
      />
    </>
  );
}

Hero.propTypes = {
  title1: PropTypes.string.isRequired,
  title2: PropTypes.string.isRequired,
  portrait: PropTypes.object.isRequired,
  intro1: PropTypes.string.isRequired,
  intro2: PropTypes.string.isRequired,
};
