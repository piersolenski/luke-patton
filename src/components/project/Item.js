import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Grid from '../common/Grid';
import Fader from '../common/Fader';
import MediaItem from '../common/MediaItem';
import Waypoint from '../common/Waypoint';

const Wrapper = styled(Fader)`
  &:not(:last-child) {
    margin-bottom: ${({ marginBottom }) => (marginBottom ? 4 : 0)}%;
  }
`;

export default function Item({ item }) {
  return (
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Wrapper
          active={active}
          marginBottom={item.primary.full_width !== 'True'}
        >
          {item.primary.full_width === 'True' ? (
            <MediaItem item={item} />
          ) : (
            <Grid>
              <MediaItem css="grid-column: span 12" item={item} />
            </Grid>
          )}
        </Wrapper>
      )}
    />
  );
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
};
