import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { H3, P, UnstyledList } from '../common/Typography';
import Grid from '../common/Grid';
import Fader from '../common/Fader';
import Waypoint from '../common/Waypoint';

import fluidRange from '../../utils/fluidRange';
import { min } from '../../utils/breakpoints';

const Wrapper = styled(Grid)`
  grid-row-gap: 30px;
  ${fluidRange({
    prop: 'margin-bottom',
    fromSize: '60px',
    toSize: '120px',
  })};
`;

const Intro = styled(Fader)`
  grid-column: span 12;
  @media (min-width: 900px) {
    grid-column: span 7;
  }
  @media (min-width: 1200px) {
    grid-column: span 6;
  }
  @media (min-width: ${min('desktop')}) {
    grid-column: span 5;
  }
`;

const Covered = styled(Fader)`
  grid-column: span 12;
  @media (min-width: 400px) {
    grid-column: span 6;
  }
  @media (min-width: 900px) {
    grid-column: span 2;
  }
  @media (min-width: 1200px) {
    grid-column: span 3;
  }
`;

const URL = styled(Fader)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  grid-column: span 12;
  @media (min-width: 400px) {
    grid-column: span 6;
  }
  @media (min-width: 900px) {
    grid-column: span 2;
  }
  @media (min-width: 1200px) {
    grid-column: span 3;
  }
  @media (min-width: ${min('desktop')}) {
    grid-column: span 4;
  }
`;

const SiteLink = styled.a`
  color: currentColor;
  text-decoration: underline;
  white-space: nowrap;
`;

const Meta = ({ intro, categories, url }) => (
  <Waypoint
    bottomOffset="20%"
    render={active => (
      <Wrapper>
        <Intro active={active} delay={0.2}>
          <H3>Intro</H3>
          <P>{intro}</P>
        </Intro>
        <Covered active={active} delay={0.3}>
          <H3>Covered</H3>
          <UnstyledList>
            {categories.map((category, i) => (
              <li key={i}>{category.category}</li>
            ))}
          </UnstyledList>
        </Covered>
        <URL active={active} delay={0.4}>
          <H3>URL</H3>
          <SiteLink href={url} target="_blank">
            {url}
          </SiteLink>
        </URL>
      </Wrapper>
    )}
  />
);

Meta.propTypes = {
  intro: PropTypes.string.isRequired,
  categories: PropTypes.array.isRequired,
  url: PropTypes.string.isRequired,
};

export default Meta;
