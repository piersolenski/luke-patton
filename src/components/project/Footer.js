import React, { forwardRef } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Img from 'gatsby-image';

import fluidRange from '../../utils/fluidRange';

import Arrow from '../icons/Arrow';
import { H2 } from '../common/Typography';
import TransitionLink from '../common/TransitionLink';
import Grid from '../common/Grid';
import { colors } from '../../utils/theme';
import Unveiler from '../common/Unveiler';

const Wrapper = styled(TransitionLink)`
  transition: ${({ visible }) => (visible ? '0s' : `var(--dur-long)`)} opacity
    ease;
  opacity: ${({ visible }) => (visible ? 1 : 0)};
  display: block;
  color: ${({ theme }) => theme.colors.white};
  background: ${({ theme }) => theme.colors.black};
  position: fixed;
  bottom: 0;
  width: 100%;
  overflow: hidden;
  ${fluidRange({
    prop: 'padding-top',
    fromSize: '30px',
    toSize: '80px',
  })};
  ${fluidRange({
    prop: 'padding-bottom',
    fromSize: '30px',
    toSize: '80px',
  })};
  ${fluidRange({
    prop: 'height',
    fromSize: '240px',
    toSize: '580px',
  })};
`;

const Preview = styled(Img)`
  position: absolute !important;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: 50% 50%;
  opacity: 0.2;
`;

const Inner = styled(Grid)`
  position: relative;
  z-index: 5;
  pointer-events: none;
  height: 100%;
`;

const ArrowIcon = styled(Arrow)`
  transition: opacity var(--dur-long) ease, transform var(--dur-long) ease;
  transform: ${({ active }) => (active ? 'translateX(0)' : 'translateX(-50%)')};
  opacity: ${({ active }) => (active ? 1 : 0)};
  ${fluidRange({
    prop: 'width',
    fromSize: '40px',
    toSize: '100px',
  })};
`;

const CTA = styled.div`
  grid-column: 1 / 13;
  display: flex;
  align-items: center;
  font-weight: normal;
  justify-content: space-between;
  align-self: start;
`;

const CTAText = styled.p`
  margin: 0 0 0 -0.5vw;
  color: currentColor;
  font-weight: 100;
  line-height: 1.2em;
  ${fluidRange({
    prop: 'font-size',
    fromSize: '40px',
    toSize: '100px',
  })};
`;

const ProjectName = styled.div`
  grid-column: 1 / span 12;
  align-self: end;
`;

const Footer = forwardRef(
  ({ image, video, active, to, title, text, visible }, ref) => {
    return (
      <Wrapper visible={visible} ref={ref} to={`/work/${to}`} preserveStyles>
        {image && !video && <Preview fluid={image.childImageSharp.fluid} />}
        {video && (
          <Preview
            as="video"
            src={video}
            poster={image && image.src}
            autoPlay
            muted
            loop
          />
        )}
        <Inner>
          <CTA>
            <Unveiler active={active}>
              <CTAText>Next Project</CTAText>
            </Unveiler>
            <ArrowIcon active={active} rotate={180} color={colors.green} />
          </CTA>
          <ProjectName>
            <Unveiler active={active}>
              <H2>{title}</H2>
            </Unveiler>
            <Unveiler active={active}>
              <H2 as="h3">{text}</H2>
            </Unveiler>
          </ProjectName>
        </Inner>
      </Wrapper>
    );
  }
);

Footer.propTypes = {
  active: PropTypes.bool.isRequired,
  image: PropTypes.object,
  text: PropTypes.any,
  title: PropTypes.any,
  to: PropTypes.string,
  video: PropTypes.string,
  visible: PropTypes.bool,
};

export default Footer;
