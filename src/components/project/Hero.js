import React from 'react';
import styled from 'styled-components';
import { format } from 'date-fns';
import PropTypes from 'prop-types';

import Grid from '../common/Grid';
import { H2 } from '../common/Typography';
import Arrow from '../icons/Arrow';

import Hr from '../common/HorizontalRule';
import Unveiler from '../common/Unveiler';
import Waypoint from '../common/Waypoint';
import TransitionLink from '../common/TransitionLink';

import fluidRange from '../../utils/fluidRange';
import { min, max } from '../../utils/breakpoints';

const Wrapper = styled(Grid)`
  margin-bottom: 24px;
  align-items: end;
  ${fluidRange({
    prop: 'padding-top',
    fromSize: '80px',
    toSize: '200px',
  })};
  @media (max-width: ${max('phablet')}) {
    padding-bottom: 10px;
  }
`;

const BackButton = styled(TransitionLink)`
  display: flex;
  align-items: center;
  color: currentColor;
  @media (max-width: ${max('phablet')}) {
    display: none;
  }
`;

const ArrowIcon = styled(Arrow)`
  @media (max-width: ${max('phablet')}) {
    display: none;
  }
  @media (min-width: ${min('phablet')}) {
    height: 45px;
    width: 30px;
  }
  transition: opacity var(--dur-long) ease, transform var(--dur-long) ease;
  opacity: ${({ active }) => (active ? 1 : 0)};
  transform: ${({ active }) => (active ? 'translateX(0)' : 'translateX(50%)')};
`;

const ProjectName = styled.div`
  @media (max-width: ${max('phablet')}) {
    grid-column: 1 / 10;
  }
  @media (min-width: ${min('phablet')}) {
    grid-column: 3 / 10;
  }
  @media (min-width: ${max('desktop')}) {
    grid-column: 6 / 11;
    display: flex;
  }
`;

const Hyphen = styled(H2)`
  @media (max-width: ${max('desktop')}) {
    display: none;
  }
`;

const Date = styled(Unveiler)`
  grid-column: 11 / 13;
  text-align: right;
  align-self: flex-end;
`;

export default function Hero({ title, strapline, date }) {
  return (
    <Waypoint
      bottomOffset="20%"
      render={active => (
        <Wrapper>
          <BackButton to="/work">
            <ArrowIcon active={active} />
          </BackButton>
          <ProjectName active={active}>
            <Unveiler active={active}>
              <H2 title={title ? 1 : 0} bold as="h1">
                {title}
              </H2>
            </Unveiler>
            <Unveiler active={active}>
              <Hyphen title={title ? 1 : 0} bold as="span">
                &nbsp;-&nbsp;
              </Hyphen>
            </Unveiler>
            <Unveiler active={active}>
              <H2 title={title ? 1 : 0} bold>
                {strapline}
              </H2>
            </Unveiler>
          </ProjectName>
          <Date active={active}>
            <H2
              title={title ? 1 : 0}
              bold
              as="time"
              dateTime={format(date, 'YYYY')}
            >
              {format(date, 'YYYY')}
            </H2>
          </Date>
          <Hr active={active} />
        </Wrapper>
      )}
    />
  );
}

Hero.propTypes = {
  title: PropTypes.string.isRequired,
  strapline: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};
