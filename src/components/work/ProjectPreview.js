import React, { useRef } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import Img from 'gatsby-image';

import { H1 } from '../common/Typography';
import Unveiler from '../common/Unveiler';
import Grid from '../common/Grid';
import TransitionLink from '../common/TransitionLink';

import useMousePosition from '../../hooks/useMousePosition';
import state from '../../state/global';

import { min, max } from '../../utils/breakpoints';

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  display: flex;
  align-items: center;
  pointer-events: ${({ active }) => (active ? 'auto' : 'none')};
`;

const Preview = styled(TransitionLink)`
  grid-row: 1;
  display: block;

  @media (max-width: ${max('phablet')}) {
    grid-column: 2 / span 10;
  }
  @media (min-width: ${min('phablet')}) {
    grid-column: 3 / span 8;
  }
  transition: clip-path var(--dur-long) cubic-bezier(0.4, 0, 0.2, 1)
    ${({ active }) => (active ? 0.8 : 0)}s;
  clip-path: ${({ active }) =>
    active
      ? 'polygon(0 0, 100% 0, 100% 100%, 0% 100%)'
      : ' polygon(0 0, 0 0, 0 100%, 0% 100%);'};
  picture > img {
    will-change: transform;
    transition: transform var(--dur-long) ease
      ${({ active }) => (active ? 0.8 : 0)}s !important;
    transform: ${({ active }) => (active ? `scale(1)` : `scale(1.2)`)};
  }
  > * {
    pointer-events: none;
  }
`;

const Time = styled.time`
  color: ${({ theme }) => theme.colors.green};
  display: block;
  @media (min-width: ${min('phablet')}) {
    margin-top: 25px;
  }
`;

const HGroup = styled.div`
  grid-row: 1;
  grid-column: 1 / span 12;
  display: flex;
  flex-direction: column;
  justify-content: center;
  pointer-events: none;
`;

export default function ProjectPreview({
  active,
  slug,
  title,
  year,
  image,
  video,
  strapline,
}) {
  const linkRef = useRef();
  const { x, y } = useMousePosition();
  const { toggleHovering } = state.useContainer();

  function checkIfHovering(e) {
    if (e.propertyName === 'clip-path') {
      const elementUnderCursor = document.elementFromPoint(x, y);
      if (elementUnderCursor === linkRef.current) {
        toggleHovering(true);
      }
    }
  }

  return (
    <Wrapper active={active}>
      <Grid>
        <Preview
          ref={linkRef}
          to={`/work/${slug}`}
          active={active}
          onTransitionEnd={e => active && checkIfHovering(e)}
        >
          {image && !video && <Img fluid={image} alt={title} />}
          {video && (
            <video
              src={video}
              poster={image && image.src}
              autoPlay
              muted
              loop
            />
          )}
        </Preview>
        <HGroup>
          <Unveiler active={active} delay={0.8}>
            <H1 as="h2">{title}</H1>
          </Unveiler>
          <Unveiler active={active} delay={0.9}>
            <H1 as="p">{strapline}</H1>
          </Unveiler>
          <Unveiler active={active} delay={1}>
            <Time>{format(year, 'YYYY')}</Time>
          </Unveiler>
        </HGroup>
      </Grid>
    </Wrapper>
  );
}

ProjectPreview.propTypes = {
  active: PropTypes.bool,
  slug: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  strapline: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  image: PropTypes.object,
  video: PropTypes.string,
};
