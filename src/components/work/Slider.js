import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Waypoint } from 'react-waypoint';

import ProjectPreview from './ProjectPreview';

import state from '../../state/global';

const FakeSection = styled.div`
  height: 100vh;
  scroll-snap-align: center;
`;

export default function Slider({ projects }) {
  const [activeProject, setActiveProject] = useState(null);
  const { pageActive } = state.useContainer();

  const onEnter = (e, i) => {
    if (e.previousPosition === 'above') {
      setActiveProject(i);
    }
  };

  const onLeave = (e, i) => {
    if (e.currentPosition === 'above' && i < projects.length - 1) {
      setActiveProject(i + 1);
    }
  };

  useEffect(() => {
    if (pageActive) {
      setTimeout(setActiveProject(0), 0);
    } else {
      setActiveProject(null);
    }
    const { style } = document.documentElement;
    Object.assign(style, {
      'scroll-snap-type': 'y mandatory',
    });

    return () => {
      Object.assign(style, {
        'scroll-snap-type': 'unset',
      });
    };
  }, [pageActive]);
  return (
    <>
      {projects.map((project, i) => (
        <Waypoint
          key={i}
          topOffset="50%"
          onEnter={e => onEnter(e, i)}
          onLeave={e => onLeave(e, i)}
        >
          <FakeSection />
        </Waypoint>
      ))}
      {projects.map((project, i) => (
        <ProjectPreview
          active={activeProject === i}
          key={project.node.uid}
          title={project.node.data.title.text}
          strapline={project.node.data.strapline.text}
          year={project.node.data.date}
          slug={project.node.uid}
          image={
            project.node.data.preview_image.localFile &&
            project.node.data.preview_image.localFile.childImageSharp.fluid
          }
          video={
            project.node.data.preview_video &&
            project.node.data.preview_video.url
          }
        />
      ))}
    </>
  );
}

Slider.propTypes = {
  projects: PropTypes.array.isRequired,
};
