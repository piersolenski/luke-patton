import React, { forwardRef } from 'react';
import Link from 'gatsby-plugin-transition-link';
import PropTypes from 'prop-types';

import state from '../../state/global';
import { getStyle, removeStyle } from '../../utils/globalStyleProps';

const TransitionLink = forwardRef(
  ({ children, preserveStyles, disableAnimation, ...props }, ref) => {
    const { setPageActive } = state.useContainer();
    const duration = Number(getStyle('--animation-duration'));

    return (
      <Link
        ref={ref}
        exit={{
          length: disableAnimation ? 0 : duration * 4,
          trigger: () => {
            setPageActive(false);
            if (!preserveStyles) {
              removeStyle('--site-background');
              removeStyle('--site-foreground');
            }
          },
        }}
        entry={{
          delay: disableAnimation ? duration : duration * 4,
          trigger: () => {
            setPageActive(true);
          },
        }}
        {...props}
      >
        {children}
      </Link>
    );
  }
);

TransitionLink.propTypes = {
  children: PropTypes.node.isRequired,
  disableAnimation: PropTypes.bool,
  preserveStyles: PropTypes.bool,
};

export default TransitionLink;
