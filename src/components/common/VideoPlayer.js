import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Player, ControlBar } from 'video-react';
import ProgressControl from './ProgressControl';

import state from '../../state/global';

import './VideoPlayer.css';

export default function VideoPlayer({ className, src }) {
  const playerRef = useRef();
  const [playbackText, setPlaybackText] = useState('Play');
  const [mouseOver, setMouseOver] = useState(false);
  const { setCursorText } = state.useContainer();

  useEffect(() => {
    // Disable full screen mode
    playerRef.current.actions.toggleFullscreen = () => null;
  });

  return (
    <div
      onMouseEnter={() => {
        setCursorText(playbackText);
        setMouseOver(true);
      }}
      onMouseLeave={() => {
        setCursorText(null);
        setMouseOver(false);
      }}
    >
      <Player
        ref={playerRef}
        className={className}
        src={src}
        onPlay={() => setPlaybackText('Pause')}
        onPause={() => setPlaybackText('Play')}
      >
        <ControlBar disableDefaultControls autoHide>
          <ProgressControl
            playbackText={playbackText}
            mouseOverPlayer={mouseOver}
          />
        </ControlBar>
      </Player>
    </div>
  );
}

VideoPlayer.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
};
