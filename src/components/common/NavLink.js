import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import TransitionLink from './TransitionLink';
import { min, max } from '../../utils/breakpoints';

const Wrapper = styled(TransitionLink)`
  color: currentColor;
  position: relative;
  @media (max-width: ${max('phablet')}) {
    font-size: 60px;
    line-height: 70px;
    display: inline-block;
  }
  @media (min-width: ${min('phablet')}) {
    font-size: 12px;
    font-weight: bold;
    display: block;
  }
  &:before {
    content: '';
    display: block;
    position: absolute;
    bottom: 0;
    height: 1px;
    background: ${({ theme }) => theme.colors.green};
    width: 0%;
    transition: width var(--dur-medium) ease;
  }
  &.active {
    color: ${({ theme }) => theme.colors.green};
    @media (min-width: ${min('phablet')}) {
      &:before {
        width: 100%;
      }
    }
  }
`;

export default function NavLink({
  path,
  url,
  name,
  onClick,
  disableAnimation,
}) {
  return (
    <Wrapper
      disableAnimation={disableAnimation}
      activeClassName="active"
      onClick={onClick}
      as={path ? TransitionLink : 'a'}
      to={path}
      href={url}
    >
      {name}
    </Wrapper>
  );
}

NavLink.propTypes = {
  disableAnimation: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  path: PropTypes.string,
  url: PropTypes.string,
};
