import styled from 'styled-components';

import fluidRange from '../../utils/fluidRange';

export const H1 = styled.h1`
  margin: 0;
  font-weight: normal;
  line-height: 1.25em;
  ${fluidRange({
    prop: 'font-size',
    fromSize: '30px',
    toSize: '60px',
  })};
`;

export const H2 = styled.h2`
  margin: 0;
  font-weight: normal;
  @media (max-width: 699px) {
    font-size: 21px;
    font-weight: ${({ bold }) => (bold ? '900' : '100')};
    line-height: ${({ title }) => (title ? '21px' : '27px')};
  }
  @media (min-width: 700px) {
    font-size: 35px;
    line-height: 45px;
  }
`;

export const H3 = styled.h3`
  font-size: 17px;
  line-height: 25px;
  @media (max-width: 699px) {
    margin: 0 0 10px;
  }
  @media (min-width: 700px) {
    margin: 0 0 20px;
  }
`;

export const P = styled.p`
  margin: 0;
  &:not(:last-child) {
    margin: 0 0 25px;
  }
`;

export const UnstyledList = styled.ul`
  list-style: none;
  padding-left: 0;
  margin: 0;
`;
