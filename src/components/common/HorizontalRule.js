import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Hr = styled.hr`
  transform: ${({ active }) => (active ? 'scaleX(1)' : 'scaleX(0)')};
  transform-origin: left;
  height: 1px;
  background: currentColor;
  border: 0;
  transition: transform var(--dur-long) cubic-bezier(0.165, 0.84, 0.44, 1);
  margin: 0;
  grid-column: span 12;
  will-change: transform;
`;

const HorizontalRule = ({ className, active }) => (
  <Hr className={className} active={active} />
);

HorizontalRule.propTypes = {
  active: PropTypes.bool,
  className: PropTypes.string,
};

HorizontalRule.defaultProps = {
  active: true,
};

export default HorizontalRule;
