import React, { useState } from 'react';
import { Waypoint } from 'react-waypoint';
import PropTypes from 'prop-types';

import state from '../../state/global';

export default function WaypointSection({
  bottomOffset,
  topOffset,
  render,
  fireOnce,
}) {
  const [visible, setVisible] = useState(false);
  const { pageActive } = state.useContainer();

  return (
    <Waypoint
      bottomOffset={bottomOffset}
      topOffset={topOffset}
      onEnter={() => setVisible(true)}
      onLeave={() => !fireOnce && setVisible(false)}
    >
      {render(pageActive ? visible : false)}
    </Waypoint>
  );
}

WaypointSection.defaultProps = {
  fireOnce: true,
};

WaypointSection.propTypes = {
  fireOnce: PropTypes.bool,
  render: PropTypes.func.isRequired,
  topOffset: PropTypes.string,
  bottomOffset: PropTypes.string,
};
