import React, { useRef } from 'react';
import { Waypoint } from 'react-waypoint';
import PropTypes from 'prop-types';

const AutoplayingVideo = ({ className, src, onLoaded }) => {
  const videoRef = useRef(null);
  return (
    <>
      <Waypoint onEnter={() => videoRef.current.play()} />
      <video
        ref={videoRef}
        className={className}
        onLoadedData={onLoaded}
        src={src}
        autoPlay
        muted
        playsInline
        loop
      />
    </>
  );
};

AutoplayingVideo.propTypes = {
  className: PropTypes.string,
  onLoaded: PropTypes.func,
  src: PropTypes.string.isRequired,
};

export default AutoplayingVideo;
