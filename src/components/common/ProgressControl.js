import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as Dom from 'video-react/lib/utils/dom';
import SeekBar from 'video-react/lib/components/control-bar/SeekBar';
import { formatTime } from 'video-react/lib/utils';
import throttle from 'lodash/throttle';

import state from '../../state/global';

export default function ProgressControl({
  playbackText,
  mouseOverPlayer,
  ...props
}) {
  const [time, setTime] = useState(0);
  const [mouseDown, setMouseDown] = useState(false);
  const [mouseEntered, setMouseEntered] = useState(false);

  const [position, setPosition] = useState(0);
  const seekBarRef = useRef();
  const { setCursorText } = state.useContainer();
  const { seekingTime } = props.store.getState().player;

  const handleMouseUp = () => setMouseDown(false);

  const handleMouseMove = throttle(updateSeekingPosition, 1000);

  useEffect(() => {
    window.addEventListener('mouseup', handleMouseUp);
    if (!mouseDown && mouseOverPlayer) {
      setCursorText(playbackText);
    }
    if (mouseEntered) {
      setCursorText('Scrub');
    }
    if (mouseDown) {
      setCursorText(formatTime(seekingTime));
    }
    if (!mouseEntered && mouseDown) {
      setCursorText(formatTime(seekingTime));
    }
    return () => window.removeEventListener('mouseup', handleMouseUp);
  }, [mouseDown, mouseEntered, seekingTime, playbackText, mouseOverPlayer]);

  function updateSeekingPosition(event) {
    if (!event.pageX) {
      return;
    }
    const {
      player: { duration },
    } = props;
    const { current: seekbar } = seekBarRef;
    const newTime = Dom.getPointerPosition(seekbar, event).x * duration;
    const newPosition = event.pageX - Dom.findElPosition(seekbar).left;
    setPosition(newPosition);
    setTime(newTime);
  }

  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div
      onMouseEnter={() => setMouseEntered(true)}
      onMouseLeave={() => setMouseEntered(false)}
      onMouseDown={() => setMouseDown(true)}
      onMouseMove={e => handleMouseMove(e)}
      className="video-react-progress-control video-react-control"
    >
      <SeekBar mouseTime={{ time, position }} ref={seekBarRef} {...props} />
    </div>
  );
}

ProgressControl.propTypes = {
  mouseOverPlayer: PropTypes.bool,
  player: PropTypes.object,
  store: PropTypes.object,
  playbackText: PropTypes.string,
};

ProgressControl.displayName = 'ProgressControl';
