import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { Waypoint } from 'react-waypoint';
import { useScrollYPosition } from 'react-use-scroll-position';
import styled from 'styled-components';

import Footer from '../project/Footer';

import state from '../../state/global';
import fluidRange from '../../utils/fluidRange';

const Wrapper = styled.div`
  position: relative;
  z-index: 5;
  background: var(--site-background);
  transition: background var(--dur-long) ease;
  ${fluidRange({
    prop: 'margin-bottom',
    fromSize: '240px',
    toSize: '580px',
  })};
  * > {
    transition: color var(--dur-long) ease;
  }
`;

const PageWithFooter = ({ children, image, video, to, title, text }) => {
  const { pageActive } = state.useContainer();
  const footerRef = useRef();
  const scrollY = useScrollYPosition();
  const [footerHeight, setFooterHeight] = useState();
  const [footerIsVisible, setFooterIsVisible] = useState(false);
  useEffect(() => {
    setFooterHeight(footerRef.current.offsetHeight);
  }, [scrollY]);

  return (
    <>
      <Wrapper>
        {children}
        <Waypoint
          onEnter={() => setFooterIsVisible(true)}
          onLeave={() => setFooterIsVisible(false)}
          bottomOffset={`${footerHeight - 100}px`}
        />
      </Wrapper>
      <Footer
        ref={footerRef}
        visible={pageActive}
        active={footerIsVisible && pageActive}
        to={to}
        image={image}
        video={video}
        title={title}
        text={text}
      />
    </>
  );
};

PageWithFooter.propTypes = {
  children: PropTypes.any,
  image: PropTypes.any,
  text: PropTypes.any,
  title: PropTypes.any,
  to: PropTypes.any,
  video: PropTypes.any,
};

export default PageWithFooter;
