import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  grid-column-gap: 2%;
  max-width: ${({ theme }) => theme.breakpoints.desktop}px;
  grid-template-columns: repeat(12, minmax(0, 100px));
  width: 94%;
  margin: 0 auto;
`;

export default Grid;
