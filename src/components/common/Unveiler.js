import styled from 'styled-components';

const Unveiler = styled.div`
  overflow: hidden;
  > * {
    display: block;
    transition: transform ease var(--dur-long)
      ${({ active, delay = 0 }) => (active ? delay : 0)}s;
    transform: ${({ active }) =>
      active ? 'translateY(0%)' : 'translateY(100%)'};
  }
`;

export default Unveiler;
