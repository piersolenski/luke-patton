import styled from 'styled-components';

const Fader = styled.div`
  > * {
    display: block;
    transition: opacity ease var(--dur-long)
      ${({ active, delay = 0 }) => (active ? delay : 0)}s;
    opacity: ${({ active }) => (active ? 1 : 0)};
  }
`;

export default Fader;
