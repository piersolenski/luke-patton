import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { graphql, StaticQuery } from 'gatsby';
import styled from 'styled-components';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import TransitionLink from './TransitionLink';

import NavLink from './NavLink';
import Grid from './Grid';
import SocialMedia from './SocialMediaNav';
import DrawerToggle from './DrawerToggle';
import Unveiler from './Unveiler';

import useBreakpoint from '../../hooks/useBreakpoint';
import { min, max } from '../../utils/breakpoints';

import state from '../../state/global';

const ROUTES = [
  { name: 'Work', path: '/work/' },
  { name: 'About', path: '/about/' },
  { name: 'Journal', path: '/journal/' },
];

const Wrapper = styled.header`
  pointer-events: none;
  position: fixed;
  z-index: 20;
  left: 0;
  top: 0;
  width: 100%;
  @media (max-width: ${max('phablet')}) {
    color: ${({ active, theme }) =>
      active ? theme.colors.white : 'currentColor'};
  }
`;

const Inner = styled(Grid)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: ${max('phablet')}) {
    height: 48px;
  }
  @media (min-width: ${min('phablet')}) {
    height: 96px;
  }
  > * {
    pointer-events: auto;
  }
`;

const Logo = styled(TransitionLink)`
  font-size: 12px;
  font-weight: bold;
  position: relative;
  color: currentColor;
  display: block;
`;

const DesktopNav = styled.nav`
  display: inline-grid;
  grid-auto-flow: column;
  grid-gap: 5vw;
  @media (max-width: ${max('phablet')}) {
    display: none;
  }
`;

const MobileNav = styled.nav`
  @media (max-width: ${max('phablet')}) {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 15;
    pointer-events: ${({ active }) => (active ? 'initial' : 'none')};
    opacity: ${({ active }) => (active ? 1 : 0)};
    transition: opacity var(--dur-short) ease
      ${({ active }) => (active ? `0s` : `var(--dur-short)`)};
    color: ${({ theme }) => theme.colors.white};
    background: ${({ theme }) => theme.colors.black};
    padding: 3%;
    display: grid;
    grid-template-rows: 1fr auto;
  }
  @media (min-width: ${min('phablet')}) {
    display: none;
  }
`;

const MobileLinks = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Toggle = styled(DrawerToggle)`
  @media (max-width: ${max('phablet')}) {
    transform: translateX(10px);
  }
  @media (min-width: ${min('phablet')}) {
    display: none;
  }
`;

export default function Header({ location }) {
  const headerRef = useRef();
  const isDesktop = useBreakpoint('phablet');
  const [visible, setVisible] = useState(false);
  const [mobileNavIsActive, setMobileNavActive] = useState(false);
  // const { objLoaded, thumbsLoaded, bustEngaged } = state.useContainer();
  // const assetsLoaded = objLoaded && thumbsLoaded;
  const { objLoaded, bustEngaged } = state.useContainer();
  const assetsLoaded = objLoaded;

  function toggleNav(bool) {
    setMobileNavActive(prevState =>
      typeof bool === 'boolean' ? bool : !prevState
    );
    if (mobileNavIsActive) {
      disableBodyScroll(headerRef.current);
    } else {
      enableBodyScroll(headerRef.current);
    }
  }

  function renderRoutes(email, active) {
    return (
      <>
        {ROUTES.map((route, i) => (
          <Unveiler key={i} active={active} delay={i * 0.1}>
            <NavLink
              name={route.name}
              path={route.path}
              url={route.url}
              onClick={() => toggleNav(false)}
              disableAnimation={!isDesktop && active}
            />
          </Unveiler>
        ))}
        <Unveiler active={active} delay={ROUTES.length * 0.1}>
          <NavLink
            name="Contact"
            url={`mailto:${email}`}
            delay={1}
            disableAnimation={!isDesktop && active}
          />
        </Unveiler>
      </>
    );
  }

  useEffect(() => {
    if (location.pathname === '/') {
      if (assetsLoaded && !bustEngaged) {
        setVisible(true);
      }
      if (assetsLoaded && bustEngaged) {
        setVisible(false);
      }
    } else {
      setTimeout(() => setVisible(true), 0);
    }
  }, [assetsLoaded, bustEngaged]);

  return (
    <StaticQuery
      query={graphql`
        query {
          prismicAbout {
            data {
              email
              social_media {
                name
                url {
                  url
                }
              }
            }
          }
        }
      `}
      render={data => (
        <>
          <Wrapper active={mobileNavIsActive} ref={headerRef}>
            <Inner>
              <Unveiler active={visible}>
                <Logo
                  active={visible}
                  to="/"
                  onClick={() => toggleNav(false)}
                  disableAnimation={!isDesktop && mobileNavIsActive}
                >
                  Luke-Patton
                </Logo>
              </Unveiler>
              <Toggle
                visible={visible}
                active={mobileNavIsActive}
                toggle={toggleNav}
              />
              <DesktopNav>
                {renderRoutes(data.prismicAbout.data.email, visible)}
              </DesktopNav>
            </Inner>
          </Wrapper>
          <MobileNav active={mobileNavIsActive}>
            <MobileLinks>
              {renderRoutes(data.prismicAbout.data.email, mobileNavIsActive)}
            </MobileLinks>
            <SocialMedia
              active={mobileNavIsActive}
              items={data.prismicAbout.data.social_media}
            />
          </MobileNav>
        </>
      )}
    />
  );
}

Header.propTypes = {
  location: PropTypes.object.isRequired,
};
