import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import UnstyledButton from './UnstyledButton';

const Wrapper = styled(UnstyledButton)`
  height: 48px;
  width: 48px;
  position: relative;
  &:focus {
    outline: none;
  }
`;

const OpenLine = styled.div`
  height: 1px;
  background: currentColor;
  position: absolute;
  right: 13px;
  will-change: opacity, transform;
  opacity: ${({ active }) => (active ? 1 : 0)};
  &:nth-child(1) {
    top: 19px;
    width: ${({ active }) => (active ? 22 : 0)}px;
    transition: all ease var(--dur-short) ${({ active }) => (active ? 0.4 : 0)}s;
  }
  &:nth-child(2) {
    top: 27px;
    width: ${({ active }) => (active ? 22 : 0)}px;
    transition: all ease var(--dur-short)
      ${({ active }) => (active ? 0.5 : 0.1)}s;
  }
`;

const ClosedLine = styled.div`
  height: 1px;
  background: ${({ theme }) => theme.colors.green};
  position: absolute;
  right: 13px;
  will-change: clip-path, opacity;
  opacity: ${({ active }) => (active ? 1 : 0.5)};
  width: 22px;
  top: 22px;
  &:nth-child(3) {
    clip-path: ${({ active }) =>
      active
        ? 'polygon(0 0, 100% 0, 100% 100%, 0 100%)'
        : 'polygon(100% 0, 100% 0, 100% 100%, 100% 100%)'};
    transform: rotate(315deg);
    transition: opacity ease var(--dur-short)
        ${({ active }) => (active ? 0.4 : 0.1)}s,
      clip-path ease var(--dur-short) ${({ active }) => (active ? 0.4 : 0.1)}s;
  }
  &:nth-child(4) {
    clip-path: ${({ active }) =>
      active
        ? 'polygon(0 0, 100% 0, 100% 100%, 0 100%)'
        : 'polygon(100% 0, 100% 0, 100% 100%, 100% 100%)'};
    transform: rotate(-135deg);
    transition: opacity ease var(--dur-short)
        ${({ active }) => (active ? 0.5 : 0)}s,
      clip-path ease var(--dur-short) ${({ active }) => (active ? 0.5 : 0)}s;
  }
`;

export default function DrawerToggle({ active, visible, toggle, className }) {
  return (
    <Wrapper className={className} onClick={toggle}>
      <OpenLine active={!active && visible} />
      <OpenLine active={!active && visible} />
      <ClosedLine active={active} />
      <ClosedLine active={active} />
    </Wrapper>
  );
}

DrawerToggle.propTypes = {
  active: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
};
