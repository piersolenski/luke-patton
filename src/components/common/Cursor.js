import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';

import { addStyle } from '../../utils/globalStyleProps';
import state from '../../state/global';

const cursorSize = {
  default: 6,
  hover: 42,
};

const Mouse = styled.div`
  width: ${({ hovering }) =>
    hovering ? `${cursorSize.hover}px` : `${cursorSize.default}px`};
  height: ${({ hovering }) =>
    hovering ? `${cursorSize.hover}px` : `${cursorSize.default}px`};
  left: ${({ hovering }) =>
    hovering ? `-${cursorSize.hover / 2}px` : `-${cursorSize.default / 2}px`};
  top: ${({ hovering }) =>
    hovering ? `-${cursorSize.hover / 2}px` : `-${cursorSize.default / 2}px`};
  border-radius: 50%;
  position: fixed;
  overflow: hidden;
  pointer-events: none;
  z-index: 999999;
  transition: background ease var(--dur-short), width ease var(--dur-short),
    height ease var(--dur-short), left ease var(--dur-short),
    top ease var(--dur-short);
  background: ${({ theme, hovering }) =>
    hovering ? `transparent` : theme.colors.green};
  border: 1px solid ${({ theme }) => theme.colors.green};
  transform: translate(var(--mouse-x), var(--mouse-y));
  justify-content: center;
  align-items: center;
  display: none;
  body:hover & {
    display: flex;
    @media (pointer: coarse) {
      display: none;
    }
  }
`;

const Text = styled.span`
  text-transform: uppercase;
  font-size: 10px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.green};
`;

export default function Cursor() {
  const links = useRef();
  const {
    hovering,
    toggleHovering,
    cursorText,
    pageActive,
  } = state.useContainer();

  useEffect(() => {
    setup();
    return () => destroy();
  }, [pageActive]);

  function updateCursorPosition(e) {
    addStyle('--mouse-x', `${e.clientX}px`);
    addStyle('--mouse-y', `${e.clientY}px`);
  }

  function setup() {
    links.current = document.querySelectorAll(
      'a, video, button, .video-react-control'
    );
    document.body.addEventListener('mouseenter', handleMouseLeave);
    document.addEventListener('mouseenter', updateCursorPosition);
    document.addEventListener('mousemove', updateCursorPosition);

    links.current.forEach(item => {
      item.addEventListener('mouseenter', handleMouseEnter);
      item.addEventListener('mouseleave', handleMouseLeave);
    });
  }

  const handleMouseEnter = () => toggleHovering(true);

  const handleMouseLeave = () => toggleHovering(false);

  function destroy() {
    document.body.removeEventListener('mouseenter', handleMouseLeave);
    document.removeEventListener('mouseenter', updateCursorPosition);
    document.removeEventListener('mousemove', updateCursorPosition);

    links.current.forEach(item => {
      item.removeEventListener('mouseenter', handleMouseEnter);
      item.removeEventListener('mouseleave', handleMouseLeave);
    });
  }

  return (
    <Mouse hovering={hovering}>{cursorText && <Text>{cursorText}</Text>}</Mouse>
  );
}
