import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';

import GlobalStyles from '../../utils/globalStyles';
import theTheme from '../../utils/theme';
import Header from './Header';

import State from '../../state/global';

import Cursor from './Cursor';

const Layout = ({ children, location }) => (
  <ThemeProvider theme={theTheme}>
    <State.Provider>
      <Cursor />
      <GlobalStyles />
      <Header location={location} />
      <main>{children}</main>
    </State.Provider>
  </ThemeProvider>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.object.isRequired,
};

export default Layout;
