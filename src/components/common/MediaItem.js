import React from 'react';
import PropTypes from 'prop-types';
import Img from 'gatsby-image';

import AutoplayingVideo from './AutoplayingVideo';
import VideoPlayer from './VideoPlayer';

const MediaItem = ({ className, item, autoPlay, onLoad }) => (
  <>
    {item.slice_type === 'image' && (
      <Img
        onLoad={onLoad}
        className={className}
        fluid={item.primary.image.localFile.childImageSharp.fluid}
        alt={item.primary.image.alt}
        fadeIn={false}
      />
    )}
    {item.slice_type === 'video' &&
      (item.primary.autoplay === 'True' || autoPlay) && (
        <AutoplayingVideo
          className={className}
          src={item.primary.video.url}
          onLoaded={onLoad}
        />
      )}
    {item.slice_type === 'video' && item.primary.autoplay === 'False' && (
      <VideoPlayer className={className} src={item.primary.video.url} />
    )}
  </>
);

MediaItem.propTypes = {
  autoPlay: PropTypes.bool,
  className: PropTypes.string,
  onLoad: PropTypes.func,
  item: PropTypes.object.isRequired,
};

export default MediaItem;
