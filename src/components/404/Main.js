import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';

import { H1, P } from '../common/Typography';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const Inner = styled.div`
  text-align: center;
`;

const CTA = styled(P)`
  margin-top: 10px;
  display: block;
  color: ${({ theme }) => theme.colors.green};
`;

const Main = () => (
  <Wrapper>
    <Inner>
      <H1>404</H1>
      <CTA as={Link}>Go back to the homepage</CTA>
    </Inner>
  </Wrapper>
);

export default Main;
