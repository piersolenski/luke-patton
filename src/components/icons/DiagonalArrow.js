import React from 'react';
import PropTypes from 'prop-types';

const DiagonalArrow = ({ color = 'currentColor', ...props }) => (
  <svg viewBox="0 0 12 11" {...props}>
    <path
      stroke={color}
      fill="none"
      strokeWidth="1"
      d="M1.216 10.153l9.35-9.351M.5.757h10.015v10.014"
    />
  </svg>
);

DiagonalArrow.propTypes = {
  color: PropTypes.string,
  strokeWidth: PropTypes.number,
};

export default DiagonalArrow;
