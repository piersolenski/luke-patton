import React from 'react';
import PropTypes from 'prop-types';

const Arrow = ({ rotate = 0, color = 'currentColor', ...props }) => (
  <svg viewBox="0 0 32.773 22.249" {...props}>
    <g
      fill="none"
      stroke={color}
      strokeWidth={2}
      transform={`rotate(${rotate}, 16.3865, 11.1245)`}
    >
      <path d="M32.774 11.024H1.407" />
      <path d="M11.831 21.541L1.414 11.124 11.831.707" />
    </g>
  </svg>
);

Arrow.propTypes = {
  color: PropTypes.string,
  rotate: PropTypes.number,
};

export default Arrow;
