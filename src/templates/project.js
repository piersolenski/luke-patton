import React, { useEffect } from 'react';
import { graphql } from 'gatsby';
import tinycolor from 'tinycolor2';
import { withTheme } from 'styled-components';
import PropTypes from 'prop-types';

import state from '../state/global';

import SEO from '../components/common/Seo';
import PageWithFooter from '../components/common/PageWithFooter';
import Hero from '../components/project/Hero';
import Meta from '../components/project/Meta';
import Item from '../components/project/Item';
import { addStyle } from '../utils/globalStyleProps';
import getNextProject from '../utils/getNextProject';

const Project = ({ data: { project, projects }, theme, location }) => {
  const { pageActive } = state.useContainer();
  const nextProject = getNextProject(projects.edges, project.uid);
  const title = nextProject.node.data.title.text;
  const { text } = nextProject.node.data.strapline;
  const slug = nextProject.node.uid;
  const image = nextProject.node.data.preview_image.localFile;
  const video = nextProject.node.data.preview_video.url;
  const { black, white } = theme.colors;
  const background = project.data.background_color;
  const color = tinycolor(background).isLight() ? black : white;

  useEffect(() => {
    addStyle('--site-foreground', color);
    addStyle('--site-background', background);
  }, [location]);

  useEffect(() => {
    if (pageActive) {
      window.scrollTo(0, 0);
    }
  }, [pageActive]);

  return (
    <PageWithFooter
      to={slug}
      image={image}
      video={video}
      title={title}
      text={text}
    >
      <SEO
        title="Work"
        image={project.data.preview_image.localFile.childImageSharp.fixed.src}
      />
      <Hero
        title={project.data.title.text}
        strapline={project.data.strapline.text}
        date={project.data.date}
      />
      <Meta
        intro={project.data.intro.text}
        categories={project.data.covered}
        url={project.data.url.url}
      />
      {project.data.body.map(item => (
        <Item item={item} key={item.id} />
      ))}
    </PageWithFooter>
  );
};

export const pageQuery = graphql`
  query($uid: String!) {
    project: prismicProject(uid: { eq: $uid }) {
      uid
      data {
        title {
          text
        }
        strapline {
          text
        }
        date
        background_color
        intro {
          text
        }
        covered {
          category
        }
        url {
          url
        }
        preview_image {
          localFile {
            childImageSharp {
              fixed(
                width: 1200
                height: 630
                cropFocus: CENTER
                quality: 80
                toFormat: PNG
              ) {
                ...GatsbyImageSharpFixed
              }
            }
          }
        }
        body {
          ... on PrismicProjectBodyImage {
            id
            slice_type
            primary {
              full_width
              image {
                alt
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1440, quality: 80) {
                      ...GatsbyImageSharpFluid_withWebp
                    }
                  }
                }
              }
            }
          }
          ... on PrismicProjectBodyVideo {
            id
            slice_type
            primary {
              autoplay
              full_width
              video {
                url
              }
            }
          }
        }
      }
    }
    projects: allPrismicProject(sort: { fields: data___date, order: DESC }) {
      edges {
        node {
          uid
          data {
            title {
              text
            }
            strapline {
              text
            }
            preview_image {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 2000, quality: 80) {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            preview_video {
              url
            }
          }
        }
      }
    }
  }
`;

Project.propTypes = {
  data: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withTheme(Project);
