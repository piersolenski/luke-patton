# Luke Patton

The portfolio of Luke Patton

## Features

- Gatsby
- Styled Components + Polished
- Prettier + ESLint integration
- SVG Spriting

## 🚀 Getting started

### Installation

Install Gatsby's command line tool.

`npm i`

### Develop

`npm run dev`

Gatsby will start a hot-reloading development environment accessible at `localhost:8000`.

### Building

`npm run build`

Gatsby will perform an optimized production build for your site generating static HTML and per-route JavaScript code bundles.

### Serving

`npm run build`

Gatsby starts a local HTML server for testing your built site.
